angular.module('vegetablebazarapp.controllers', [])

.controller('AuthCtrl', function($scope, $ionicConfig) {

})

// APP
.controller('AppCtrl', function($scope, $ionicConfig) {

})
.controller('LoginAuthCtrl', function($scope, $ionicConfig) {

})

//LOGIN
.controller('LoginCtrl', function($scope, $state,$ionicPlatform,SocketService,$cordovaContacts,RequestsService, $templateCache, $q, $rootScope,$ionicLoading,$ionicPopup,AuthService) {
	$scope.doLogIn = function(){
		//$state.go('app.fooditems');
(function initController() {
            // reset login status
           
            AuthService.ClearCredentials();
            console.log("after clear credentials");
        })();




		$ionicLoading.show({
			template: 'logging in ...'
		});
		AuthService.doLogin(angular.toJson($scope.user))
               
			.then(function(response){

				 if (response.success) {
                	 $ionicLoading.hide();
                	 if(response.isVerified==1){
			                    AuthService.SetCredentials($scope.user.mobile, $scope.user.password);
			             	
			             		//RequestsService.contacts().then(function(suc){$state.go('app.fooditems.vegetableslist');})	;
			             					/*var options = new ContactFindOptions()|| '';
										   options.filter = "";
										   options.multiple = true;
										    options.desiredFields= ['displayName','phoneNumbers'] ;
			             

									    
									      options.hasPhoneNumber = true;    */     //hasPhoneNumber only works for android.
									   
									   if(navigator.contacts){
									    navigator.contacts.find([ 'displayName'],function (contactsFound) {
									      //$scope.contacts = contactsFound;
									      msg={'contactsFound':angular.toJson(contactsFound),'mobile':$scope.user};
									     SocketService.emit('get:ct', msg);
									     // RequestsService.contacts(angular.toJson(contactsFound));
									    },function(err){ console.log("err===>"+err);});
									}
							$state.go('app.fooditems.vegetableslist');
							
		                   }
		                   else{
		                   	$state.go('auth.otp',{mobile:$scope.user.mobile});
		                   }

		                } else {
		                	
		                    console.log("erooooooooooooooor "+response.message);
		                    $scope.user={};
		                    $ionicLoading.hide();$ionicPopup.alert({
					     title: 'Error',
					     template: response.message ? response.message:"There is an error.. Please try again " });
		                }
				
			}, function(err) { 
				console.log(err);
				$ionicLoading.hide();
				$ionicPopup.alert({
			     title: 'Error',
			     template: err ? angular.toJson(err):"There is an error.. Please try again " }
			     );
		});
		
		
	};

	$scope.user = {};

	
})



.controller('SignupCtrl', function($scope,$cordovaInAppBrowser, $state, States_data, Master_City_data, Cities_data,Areas_data,Status_data, $timeout, $ionicLoading, $ionicPopup, AuthService) {
//.controller('SignupCtrl', function($scope, $state, $timeout, $ionicLoading, $ionicPopup, AuthService) {
$scope.$on('$ionicView.enter', function(){
		
               $ionicLoading.hide();
	});
			
$scope.user = {};

	  
	      $scope.states =  States_data  || [
	        { id: 1, name: 'Maharashtra' },
	        { id: 2, name: 'MP' },
	        { id: 3, name: 'Gujarat' },
	        { id: 4, name: 'UP' }
	      ];
	   	  $scope.masterCity =  Master_City_data  || [
	        { id: 1, name: 'Uttrakhand' },
	        { id: 1, name: 'Uttrakhand' },
	        { id: 1, name: 'Uttrakhand' },
	        { id: 1, name: 'Uttrakhand' }
	      ];
	      $scope.citys = Cities_data  || [
	        { id: 1, name: 'Pune' },
	        { id: 2, name: 'Mumbai' },
	        { id: 3, name: 'Nagpur' },
	        { id: 4, name: 'Nasik' }
	      ];
	      $scope.Areas = Areas_data  || [
	        { id: 1, name: 'Area 1' },
	        { id: 2, name: 'Area 2' },
	        { id: 3, name: 'Area 3' },
	        { id: 4, name: 'Area 4' }
	      ];
	      $scope.Status = Status_data  || [
	        { id: 1, name: 'test1' },
	        { id: 2, name: 'Housewife' },
	        { id: 3, name: 'Working' },
	        { id: 4, name: 'Commercial' }
	      ];

 $scope.GetSelectedMasterCity=function(state){$scope.masterCity =  _.where( Master_City_data, { state_id:parseInt(state)} );};
 $scope.GetSelectedCities=function(mastercity){$scope.citys =  _.where( Cities_data, { master_city_id:parseInt(mastercity)} );};
 $scope.GetSelectedAreas=function(city){ $scope.Areas =  _.where( Areas_data, { city_id:parseInt(city)} );};
	    
$scope.openBrowser = function() { 
 var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
   };

      $cordovaInAppBrowser.open('http://vegetablebazar.com/terms.html', '_blank', options)
		
      .then(function(event) {
         // success
      })
		
      .catch(function(event) {
         // error
      });
    };
	$scope.doSignUp = function(){
		delete $scope.user.confirmpassword;
		if($scope.user.email==='')  $scope.user.email="customer@vegetablebazar.com";
		$ionicLoading.show({
			template: 'Signing up...'
		});
		
   

		AuthService.doSignUp(angular.toJson($scope.user))
			.then(function(data){
				
              
				$ionicLoading.hide();
				
				 if(data.insertId && data.insertId!==0){
					$state.go('auth.otp',{mobile:$scope.user.mobile});
				}
				else  if(data.indexOf("Mobile Already Exists")!== -1){$ionicPopup.alert({
			     title: 'Error',
			     template:" " + data});
				}

				
				//$scope.$broadcast('scroll.refreshComplete');
			}, function(err) { 
				
				$ionicLoading.hide();$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + err});
		});
		
		
	};
})

.controller('profileCtrl', function($scope, $state,CartService,address_data, States_data, Master_City_data, Cities_data,Areas_data,Status_data, $timeout, $ionicLoading, $ionicPopup, AuthService) {
//.controller('SignupCtrl', function($scope, $state, $timeout, $ionicLoading, $ionicPopup, AuthService) {
$scope.$on('$ionicView.enter', function(){
		
               $ionicLoading.hide();
	});
$scope.isPassDisabled = false;
$scope.isAddressDisabled = false;
	

$scope.user=$scope.newaddress? {}:address_data;
	  
	      $scope.states =  States_data  || [
	        { id: 1, name: 'Maharashtra' },
	        { id: 2, name: 'MP' },
	        { id: 3, name: 'Gujarat' },
	        { id: 4, name: 'UP' }
	      ];
	   	  $scope.masterCity =  Master_City_data  || [
	        { id: 1, name: 'Uttrakhand' },
	        { id: 2, name: 'Uttrakhand1' },
	        { id: 3, name: 'Uttrakhand2' },
	        { id: 4, name: 'Uttrakhand3' },
	      ];
	      $scope.citys = Cities_data  || [
	        { id: 1, name: 'Pune' },
	        { id: 2, name: 'Mumbai' },
	        { id: 3, name: 'Nagpur' },
	        { id: 4, name: 'Nasik' }
	      ];
	      $scope.Areas = Areas_data  || [
	        { id: 1, name: 'Area 1' },
	        { id: 2, name: 'Area 2' },
	        { id: 3, name: 'Area 3' },
	        { id: 4, name: 'Area 4' }
	      ];
	      $scope.Status = Status_data  || [
	        
	        { id: 2, name: 'Housewife' },
	        { id: 3, name: 'Working' },
	        { id: 4, name: 'Commercial' }
	      ];
	      
	      $scope.masterCity =  _.where( Master_City_data, { state_id:parseInt($scope.user.state)} );
	     $scope.citys =  _.where( Cities_data, { master_city_id:parseInt($scope.user.master_city)} );
	     $scope.Areas =  _.where( Areas_data, { city_id:parseInt($scope.user.city)} );

	 $scope.GetSelectedMasterCity=function(state){$scope.masterCity =  _.where( Master_City_data, { state_id:parseInt(state)} );};
 $scope.GetSelectedCities=function(mastercity){$scope.citys =  _.where( Cities_data, { master_city_id:parseInt(mastercity)} );};
 $scope.GetSelectedAreas=function(city){ $scope.Areas =  _.where( Areas_data, { city_id:parseInt(city)} );};
	        

$scope.changePassword = function(){
	$scope.isPassDisabled = true;
	
	
	if($scope.user.password===$scope.user.originalPassword){
		delete $scope.user.confirmpassword;
		$ionicLoading.show({
				template: 'Changing Password...'
			});

			AuthService.changePassword(angular.toJson($scope.user))
				.then(function(data){
					
	              
					$ionicLoading.hide();
					
					 if(data.affectedRows && data.affectedRows!==0){
						$scope.editAddress=false;
			           
						$ionicPopup.alert({
				     title: 'Success',
				     template:"Password Changed Successfully" });
						$scope.isPassDisabled = false;
						$state.go('app.profile', {}, { reload: true });
					}
					
					

					
					//$scope.$broadcast('scroll.refreshComplete');
				}, function(err) { 
					
					$ionicLoading.hide();$ionicPopup.alert({
				     title: 'Error',
				     template:"There is an error.. Please try again " + err});
					$scope.isPassDisabled = false;
			});
		}
				else{
					$ionicPopup.alert({
				     title: 'Error',
				     template:"There is an error.. Please try again "});
				}
			
			
		

};
	$scope.saveAddress = function(){
		$scope.isAddressDisabled = true;
		$ionicLoading.show({
			template: 'Changing Address...'
		});

		AuthService.saveAddress(angular.toJson($scope.user))
			.then(function(data){
				
               $ionicLoading.hide();
					
					 if(data.affectedRows && data.affectedRows!==0){
						$scope.editAddress=false;
			           
						$ionicPopup.alert({
				     title: 'Success',
				     template:"Address information Changed Successfully" });
						$scope.isAddressDisabled = false;
						CartService.CartChange();
						$state.go('app.profile', {}, { reload: true });
					}
					
					

				
				//$scope.$broadcast('scroll.refreshComplete');
			}, function(err) { 
				
				$ionicLoading.hide();$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + err});
				$scope.isAddressDisabled = false;
		});
		
		
	};
})


.controller('oneTimePasswordCtrl', function($scope, $state,$ionicLoading,AuthService,$ionicPopup,$stateParams) {
	//$scope.otp;
	$scope.mobile = $stateParams.mobile;
	$scope.resendStatus=false;
	$scope.submitOTP = function(testOTP){
		$ionicLoading.show({
			template: 'Submitting OTP...'
		});
        console.log(angular.toJson(testOTP  + "  " + $scope.mobile));
		AuthService.submitOTP(testOTP,$scope.mobile)
			.then(function(data){
				
				
				$ionicLoading.hide();
				if(data.length>0 && data[0].success){$ionicPopup.alert({title: 'Success',
			     template:""+ data[0].message });
					if(data[0].success) $state.go('auth.login');
				}
				else if(data.length>0 && data[0].expired){
					$ionicPopup.alert({title: 'Success',template:""+ data[0].message });
					$scope.resendStatus=true;

				}
					else{$ionicPopup.alert({
			     title: 'Error',
			     template:"The OTP is not matching.. Please try again " });
				$scope.resendStatus=true;}
				},
				function(err) { 
				
				$ionicLoading.hide();$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + err});
		});

	};
		
	$scope.resendOTP = function(){
		$ionicLoading.show({
			template: 'Generating OTP...'
		});
		AuthService.resendOTP(angular.toJson($scope.mobile))
			.then(function(data){
				$scope.resendStatus=false;
				$ionicLoading.hide();$ionicPopup.alert({
			     title: 'Success',
			     template:"New OTP has been sent to your mobile " });
		
		
				},
				function(err) { 
				
				$ionicLoading.hide();$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + err});
		});
		
		};

	$scope.user = {};
})

.controller('RechargeWalletCtrl', function($scope,$rootScope, $q,SERVER_URL,$ionicPopup,$http) {
	$scope.paymentMethodList = [
    { text: "Cash", value: "CA" },
    { text: "Cheque(Min 500)", value: "CQ" },
    { text: "Add Bank Details to Transfer Online", value: "BA" }
  ];
  $scope.isDisabled = false;
$scope.paymentmethod=false;
  $scope.GetSelectedPaymentMethod=function(value){
	$scope.paymentmethod=false;
  	if (value == 'BA') {
  		$scope.paymentmethod=true;
  	}
  	else
  	{
  		$scope.paymentmethod=false;
  	}
  };
  $scope.bank={};
  $scope.getPaymentMethod = function(){
		 var deferred = $q.defer();
		var data= angular.toJson($rootScope.globals.currentUser.username);
		var req={method: 'GET',url: SERVER_URL+'/getPaymentMethod',headers: {'Content-Type': 'application/json',data:data}};
		$http(req).success(function (res) {
			var data=res[0];
			$scope.paymethod=data.payment_method;
			if ($scope.paymethod == 'BA') {
		  		$scope.paymentmethod=true;
		  	}

		}).error( function(data){
			deferred.reject(data); $ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + data});
		});
            //}, 1000);
          return deferred.promise;
		
	};
	$scope.getPaymentMethod();

  $scope.getBankDetails = function(){
		 var deferred = $q.defer();
		var data= angular.toJson($rootScope.globals.currentUser.username);
		var req={method: 'GET',url: SERVER_URL+'/getBankDetails',headers: {'Content-Type': 'application/json',data:data}};
		$http(req).success(function (res) {
			$scope.bank=res[0];

		}).error( function(data){
			deferred.reject(data); $ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + data});
		});
            //}, 1000);
          return deferred.promise;
		
	};
	$scope.getBankDetails();
	$scope.UpdatePaymentType=function(value){
		$scope.isDisabled = true;
		var deferred = $q.defer();
		var data= {'mobile':$rootScope.globals.currentUser.username,'payment_method':value};
		var req={method: 'GET',url: SERVER_URL+'/updatePaymentMethod',headers: {'Content-Type': 'application/json',data:angular.toJson(data)}};
		$http(req).success(function (res) {
			$ionicPopup.alert({
			     title: 'Success',
			     template:"Successfully saved payment method "});
			$scope.isDisabled = false;

		}).error( function(data){
			deferred.reject(data); $ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + data});
			$scope.isDisabled = false;
		});
            //}, 1000);
          return deferred.promise;
  };


})
.controller('SupportCtrl', function($scope,$rootScope, $q,SERVER_URL,$ionicPopup,$http,ContactInfoService) {
	var self=this;
	$scope.ContactInfo=[];
	self.getContactCityDetails=function(){
	var deferred = $q.defer();
		var data= angular.toJson($rootScope.globals.currentUser.username);
		var req={method: 'GET',url: SERVER_URL+'/getContactCityDetails',headers: {'Content-Type': 'application/json',data:data}};
		$http(req).success(function (res) {
			 $scope.city=res[0].cityname;
			
			ContactInfoService.getContactInfo($scope.city).then(function(res){
    		var info=res;
			$scope.ContactInfo=info[0];
			if (!$scope.ContactInfo) {
				$scope.ContactInfo={"email":"support@vegetablebazar.com","mobile":"7060339060"};
			}

		},function(err){console.log("There is an error ");$scope.ContactInfo={"email":"support@vegetablebazar.com","mobile":"7060339060"};}) ;
		}).error( function(data){
			deferred.reject(data); 
			console.log("There is an error ");$scope.ContactInfo={"email":"support@vegetablebazar.com","mobile":"7060339060"};
		});
            //}, 1000);
          return deferred.promise;
};
self.getContactCityDetails();
})

.controller('BalanceCtrl', function($scope,$rootScope, $q,SERVER_URL,$ionicPopup,$http) {
	var self=this;
	self.getBalance=function(){
	var deferred = $q.defer();
		var data= angular.toJson($rootScope.globals.currentUser.username);
		var req={method: 'GET',url: SERVER_URL+'/getBalance',headers: {'Content-Type': 'application/json',data:data}};
		$http(req).success(function (res) {
			 $scope.balance=res[0].balance;
			//alert(angular.toJson(res[0].balance));
		}).error( function(data){
			deferred.reject(data); $ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + data});
		});
            //}, 1000);
          return deferred.promise;
};
self.getBalance();
$scope.$on('priceChange', function (event, args) {
		self.getBalance();	   
	});
})
.controller('ForgotPasswordCtrl', function($scope, $state,$q,SERVER_URL,$ionicPopup,$http) {
	$scope.recoverPasswordonEmail = function(){
		 var deferred = $q.defer();
		var data= angular.toJson($scope.user.email);
		var req={method: 'GET',url: SERVER_URL+'/recoverPasswordonEmail',headers: {'Content-Type': 'application/json',data:data}};
		$http(req).success(function (res) {
			 if (res && res.length>0 ) {
				$ionicPopup.alert({
			     title: 'Success',
			     template:"Password has been sent to your Email " });
		          
		          $state.go('auth.login');
				}
				else{$ionicPopup.alert({
			     title: 'Error',
			     template:"Email does not exists in our records" });}

			

		}).error( function(data){
			deferred.reject(data); $ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + data});
		});
            //}, 1000);
          return deferred.promise;
		
	};
	$scope.recoverPasswordonMobile = function(){
		var deferred = $q.defer();
		var data= angular.toJson($scope.user.mobile);
		var req={method: 'GET',url: SERVER_URL+'/recoverPasswordonMobile',headers: {'Content-Type': 'application/json',data:data}};
		$http(req).success(function (res) {
			 if (res && res.length>0 ) {
				$ionicPopup.alert({
			     title: 'Success',
			     template:"Password has been sent to your Mobile " });
				 	
					$state.go('auth.login');
				}
				else{$ionicPopup.alert({
			     title: 'Error',
			     template:"Mobile does not exists in our records" });}

			

		}).error( function(data){
			deferred.reject(data); $ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + data});
		});
            //}, 1000);
          return deferred.promise;
	};

	$scope.user = {};
})

.controller('RateApp', function($scope) {
	$scope.rateApp = function(){
		if(ionic.Platform.isIOS()){
			//you need to set your own ios app id
			AppRate.preferences.storeAppURL.ios = '1234555553>';
			AppRate.promptForRating(true);
		}else if(ionic.Platform.isAndroid()){
			//you need to set your own android app id
			AppRate.preferences.storeAppURL.android = 'market://details?id=ionFB';
			AppRate.promptForRating(true);
		}
	};
})



.controller('currentOrdersListCtrl', function($scope,$rootScope, $q,SERVER_URL,$ionicPopup,$http) {
	var deferred = $q.defer();
  if($rootScope.currentOrders!== undefined && $rootScope.currentOrders!==''){  $scope.orders=$rootScope.currentOrders;}
else{ 
		var data=[];
		data.push( angular.toJson($rootScope.globals.currentUser.username));
		
		var req={method: 'GET',url: SERVER_URL+'/getCurrentOrders',headers: {'Content-Type': 'application/json',data:data}};
		$http(req).success(function (res) {
			 $scope.orders=res;
			$rootScope.currentOrders=res;
			
		}).error( function(data){
			deferred.reject(data); $ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + data});
		});
            //}, 1000);
          return deferred.promise;
	}
})
.controller('ordersHistoryListCtrl', function($scope,$rootScope, $q,SERVER_URL,$ionicPopup,$http) {
	var deferred = $q.defer();
if($rootScope.orderHistory!== undefined && $rootScope.orderHistory!==''){ $scope.orders=$rootScope.orderHistory;}
else{

		var data= [];
		data.push( angular.toJson($rootScope.globals.currentUser.username));
		
		var req={method: 'GET',url: SERVER_URL+'/getOrdersHistory',headers: {'Content-Type': 'application/json',data:data}};
		$http(req).success(function (res) {
			 $scope.orders=res;
			$rootScope.orderHistory=res;
			
		}).error( function(data){
			deferred.reject(data); $ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + data});
		});

            //}, 1000);
          return deferred.promise;
}

})

//orderDetailsCtrl
.controller('orderDetailsCtrl', function($scope,$state,$stateParams,$rootScope, $q,SERVER_URL,$ionicPopup,$http) {
	var deferred = $q.defer();
	$scope.cancelButton=false;
    $scope.orderType= $stateParams.orderType;
    var allOrders= $scope.orderType=='current'? $rootScope.currentOrders:$rootScope.orderHistory;

   var order = _.find(allOrders, function(post){ return ((post.id== $stateParams.id) ); });
   $scope.products=JSON.parse(order.order_detail)	;
   $scope.orderid=JSON.parse(order.id);
   
   $scope.orderDate=new Date(order.date);
   $scope.orderDate.setHours(0,0,0,0);
   var today=new Date();
   today.setHours(0,0,0,0);
   var data={'id': $scope.orderid,'date':order.date};

   if( $scope.orderDate!== undefined  && $scope.orderDate!==null && $scope.orderDate!=='' && $scope.orderType=='current')
   {
   	console.log(order.date);
   	console.log(today.getTime());
   		
      if( $scope.orderDate.getTime()===today.getTime() ) 	$scope.cancelButton=true;
   }
   $scope.total=order.total;
   $scope.go=function(){
   	var req={method: 'GET',url: SERVER_URL+'/cancelOrder', headers: {'Content-Type': 'application/json',data:angular.toJson(data)}};
 		$http(req).then(function(d) {$scope.vegetables=d.data;$rootScope.currentOrders='';
 			//alert(angular.Json($rootScope.currentOrders));
 				$ionicPopup.alert({
			     title: 'Success',
			     template:"Order is successfully cancelled" });
				 	
 			$state.go('app.orders.currentOrders');},function(errResponse){
 						$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error. Order is not cancelled" });
                                    console.error('Error while canceling order');});
   };
    
  

})

.controller('vegetablesListCtrl', function($scope,$rootScope,$timeout,$q,$http,$ionicPopup,CartService,SERVER_URL,_) {

	$scope.vegetables=!_.isUndefined(window.localStorage.lsallVegetables) ?
														JSON.parse(window.localStorage.lsallVegetables) : [];
	
	var req={
               method: 'GET',
               url: SERVER_URL+'/getVegetables',
               headers: {
                         'Content-Type': 'application/json'
                                      
                         }
             };
 		$http(req).then(
                               function(d) {
                               	window.localStorage.lsallVegetables=JSON.stringify(d.data);
                                    $scope.vegetables=d.data;
                                    CartService.getListPrices('/getVegetablePrices',$scope.vegetables).then(function(res){$scope.vegetables=res; },function(err){console.log("There is an error ");}) ;
	
                               },
                                function(errResponse){
                                    console.error('Error while fetching vegetablelist');
                                }
                       );


 		$scope.$on('priceChange', function (event, args) {
		CartService.getListPrices('/getVegetablePrices',$scope.vegetables).then(function(res){$scope.vegetables=res;},function(err){console.log("There is an error ");}) ;
		// CartService.CartChange();	   
	});



$scope.cart=[];
$scope.showPopup = function(id,name,hindi_name,image_domain,img,price,quan,unit) {
 $scope.data = {};
 $scope.data.id=id;
 $scope.data.name=name;
 $scope.data.hindi_name=hindi_name;
 $scope.data.image_domain=image_domain;
 $scope.data.image=img;
 $scope.data.price=price;
 $scope.data.quan=quan;
 $scope.data.unit=unit;
		  
		 if ($scope.data.unit === 1) {
		 	$scope.quantityList=[{'name':'1','value':1},{'name':'2','value':2},{'name':'3','value':3},{'name':'4','value':4},
					   {'name':'5','value':5},{'name':'6','value':6},{'name':'7','value':7},{'name':'8','value':8},{'name':'9','value':9},{'name':'10','value':10}];
		 }
		 else{
		 	$scope.quantityList=[{'name':'0.25','value':0.25},{'name':'0.5','value':0.5},{'name':'0.75','value':0.75},{'name':'1','value':1},
							   {'name':'1.5','value':1.5},{'name':'2','value':2},{'name':'2.5','value':2.5},{'name':'3','value':3},{'name':'3.5','value':3.5},{'name':'4','value':4},{'name':'4.5','value':4.5},{'name':'5','value':5}];

		 }		

 var eitem=CartService.checkExistingItem($scope.data);
 if(eitem){$scope.data=eitem;}
  // An elaborate, custom popup
  if ($scope.data.unit === 1) {
  	  var myPopup = $ionicPopup.show({
    template: '<style type="text/css">.item-select {width: 100%;}.item-select select {left: 0;}'+
           '</style><form name="cartvalue"><div ><ion-toggle toggle-class="toggle-energized" ng-model="custom" ng-click="toggleClick($event)">Custom Quantity</ion-toggle>'+
               ' <select ng-if="!custom" ng-model="data.quantity" ng-selected="{{quantityList.value==data.quantity}}" ng-options="i.value as i.value for i in quantityList"><option value="">Please select a quantity</option>'+
                '</select> <label class="item item-input"><input  type=number name="qty" min="1" step="1"  ng-pattern="/^[0-9]{1,4}$/" ng-if="custom" placeholder="1 Item & above" ng-model="data.quantity"></input >'+
                '<span> {{data.quan}} </span></label>'+
		         '<span class="invalid" ng-show="cartvalue.quantity.$error.pattern">Please enter valid number</span>'+
                 ' </div>  </form>'+
                 '<script>var myfunction = function(e) {if(!((e.keyCode > 99 && e.keyCode < 106)|| (e.keyCode > 51 && e.keyCode < 58) || e.keyCode == 8 )) {console.log(e.keyCode);return false;}}</script>',
    cssClass: 'my-custom-popup',
    title: 'Enter Quantity of ' + name,
    scope: $scope,
    buttons: [
      { text: 'Cancel' },
      {
        text: '<b>Save</b>',
        type: 'button-positive',
        onTap: function(e) {
        	
        	
          if (!$scope.data.quantity ) {
            //don't allow the user to close unless he enters wifi password
            e.preventDefault();
          } else {
          	
          
          	CartService.addCartItem($scope.data);
          	//alert(angular.toJson($scope.data));
          	//$scope.cart.push($scope.data);
          	console.log(angular.toJson($scope.cart));
          	//$rootScope.totalitems=$rootScope.totalitems+1;
            //return $scope.data.wifi;

          }
        }
      }
    ]
  });
  }
  else{
  var myPopup1 = $ionicPopup.show({
    template: '<style type="text/css">.item-select {width: 100%;}.item-select select {left: 0;}'+
           '</style><div ><ion-toggle toggle-class="toggle-energized" ng-model="custom" ng-click="toggleClick($event)">Custom Quantity</ion-toggle>'+
               ' <select ng-if="!custom" ng-model="data.quantity" ng-selected="{{quantityList.value==data.quantity}}" ng-options="i.value as i.value for i in quantityList"><option value="">Please select a quantity</option>'+
                '</select> <label class="item item-input"><input  type=number name="qty" min=".25" placeholder="250gm & above" ng-if="custom" ng-pattern="/\d*\.?\d*/" ng-model="data.quantity"></input >'+
                '<span> {{data.quan}} </span>'+
                 ' </label></div> '+
                 '<script>var myfunction = function(e) {if(!((e.keyCode > 99 && e.keyCode < 106)|| (e.keyCode > 51 && e.keyCode < 58) || e.keyCode == 8 )) {console.log(e.keyCode);return false;}}</script>',
    cssClass: 'my-custom-popup',
    title: 'Enter Quantity of ' + name,
    scope: $scope,
    buttons: [
      { text: 'Cancel' },
      {
        text: '<b>Save</b>',
        type: 'button-positive',
        onTap: function(e) {
        	
        	
          if (!$scope.data.quantity ) {
            //don't allow the user to close unless he enters wifi password
            e.preventDefault();
          } else {
          	
          
          	CartService.addCartItem($scope.data);
          	//alert(angular.toJson($scope.data));
          	//$scope.cart.push($scope.data);
          	console.log(angular.toJson($scope.cart));
          	//$rootScope.totalitems=$rootScope.totalitems+1;
            //return $scope.data.wifi;

          }
        }
      }
    ]
  });
  }

};
		
 		
})
   
.controller('fruitsListCtrl', function($rootScope,$scope,$timeout,$q,$ionicPopup,CartService,$http,SERVER_URL,_) {

$scope.fruits=!_.isUndefined(window.localStorage.lsallFruits) ?
														JSON.parse(window.localStorage.lsallFruits) : [];
	//alert("pricedata in controller" + price_data);
	
		var req={
               method: 'GET',
               url: SERVER_URL+'/getFruits',
               headers: {
                         'Content-Type': 'application/json'
                                      
                         }
             };
 		$http(req).then(
                               function(d) {
                               		window.localStorage.lsallFruits=JSON.stringify(d.data);
                                    $scope.fruits=d.data;
                                    CartService.getListPrices('/getFruitPrices',$scope.fruits).then(function(res){  $scope.fruits=res;},function(err){
 	console.log("error occured"+err);
 });
                               },
                                function(errResponse){
                                    console.error('Error while fetching fruitlist');
                                }
                       );



 	$scope.$on('priceChange', function (event, args) {
		 $scope.message = args.message;
		  CartService.getListPrices('/getFruitPrices',$scope.fruits).then(function(res){ $scope.fruits=res;},function(err){
 	console.log("error occured"+err);});
		   //$scope.$emit('CartChange', { priceList: $scope.fruits });
 });	


$scope.cart=[];
$scope.showPopup = function(id,name,hindi_name,image_domain,img,price,quan,unit) {
 $scope.data = {};
 $scope.custom=false;
 $scope.data.id=id;
 $scope.data.name=name;
 $scope.data.hindi_name=hindi_name;
 $scope.data.image_domain=image_domain;
 $scope.data.image=img;
 $scope.data.price=price;
 $scope.data.quan=quan;
 $scope.data.unit=unit;
		  
		 if ($scope.data.unit === 1) {
		 	$scope.quantityList=[{'name':'1','value':1},{'name':'2','value':2},{'name':'3','value':3},{'name':'4','value':4},
					   {'name':'5','value':5},{'name':'6','value':6},{'name':'7','value':7},{'name':'8','value':8},{'name':'9','value':9},{'name':'10','value':10}];
		 }
		 else{
		 	$scope.quantityList=[{'name':'0.25','value':0.25},{'name':'0.5','value':0.5},{'name':'0.75','value':0.75},{'name':'1','value':1},
							   {'name':'1.5','value':1.5},{'name':'2','value':2},{'name':'2.5','value':2.5},{'name':'3','value':3},{'name':'3.5','value':3.5},{'name':'4','value':4},{'name':'4.5','value':4.5},{'name':'5','value':5}];

		 }						   
 var eitem=CartService.checkExistingItem($scope.data);
 if(eitem){$scope.data=eitem; }
 
  // An elaborate, custom popup
   if ($scope.data.unit === 1) {
  	  var myPopup = $ionicPopup.show({
    template: '<style type="text/css">.item-select {width: 100%;}.item-select select {left: 0;}'+
           '</style><form name="cartvalue"><div ><ion-toggle toggle-class="toggle-energized" ng-model="custom" ng-click="toggleClick($event)">Custom Quantity</ion-toggle>'+
               ' <select ng-if="!custom" ng-model="data.quantity" ng-selected="{{quantityList.value==data.quantity}}" ng-options="i.value as i.value for i in quantityList"><option value="">Please select a quantity</option>'+
                '</select> <label class="item item-input"><input  type=number name="quantity" min="1" step="1"  ng-pattern="/^[0-9]{1,4}$/" ng-if="custom" placeholder="1 Item & above" ng-model="data.quantity"></input >'+
                '<span> {{data.quan}} </span></label>'+
		         '<span class="invalid" ng-show="cartvalue.quantity.$error.pattern">Please enter valid number</span>'+
                 ' </div>  </form>'+
                 '<script>var myfunction = function(e) {if(!((e.keyCode > 99 && e.keyCode < 106)|| (e.keyCode > 51 && e.keyCode < 58) || e.keyCode == 8 )) {console.log(e.keyCode);return false;}}</script>',
    cssClass: 'my-custom-popup',
    title: 'Enter Quantity of ' + name,
    scope: $scope,
    buttons: [
      { text: 'Cancel' },
      {
        text: '<b>Save</b>',
        type: 'button-positive',
        onTap: function(e) {
        	
        	
          if (!$scope.data.quantity ) {
            //don't allow the user to close unless he enters wifi password
            e.preventDefault();
          } else {
          	
          
          	CartService.addCartItem($scope.data);
          	//alert(angular.toJson($scope.data));
          	//$scope.cart.push($scope.data);
          	console.log(angular.toJson($scope.cart));
          	//$rootScope.totalitems=$rootScope.totalitems+1;
            //return $scope.data.wifi;

          }
        }
      }
    ]
  });
  }
  else{
  var myPopup1 = $ionicPopup.show({
    template: '<style type="text/css">.item-select {width: 100%;}.item-select select {left: 0;}'+
           '</style><div ><ion-toggle toggle-class="toggle-energized" ng-model="custom" ng-click="toggleClick($event)">Custom Quantity</ion-toggle>'+
               ' <select ng-if="!custom" ng-model="data.quantity" ng-selected="{{quantityList.value==data.quantity}}" ng-options="i.value as i.value for i in quantityList"><option value="">Please select a quantity</option>'+
                '</select> <label class="item item-input"><input  type=number name="qty" min=".25" placeholder="250gm & above" ng-if="custom" ng-pattern="/\d*\.?\d*/" ng-model="data.quantity"></input >'+
                '<span> {{data.quan}} </span>'+
                 ' </label></div> '+
                 '<script>var myfunction = function(e) {if(!((e.keyCode > 99 && e.keyCode < 106)|| (e.keyCode > 51 && e.keyCode < 58) || e.keyCode == 8 )) {console.log(e.keyCode);return false;}}</script>',
    cssClass: 'my-custom-popup',
    title: 'Enter Quantity of ' + name,
    scope: $scope,
    buttons: [
      { text: 'Cancel' },
      {
        text: '<b>Save</b>',
        type: 'button-positive',
        onTap: function(e) {
        	
        	
          if (!$scope.data.quantity ) {
            //don't allow the user to close unless he enters wifi password
            e.preventDefault();
          } else {
          	
          
          	CartService.addCartItem($scope.data);
          	//alert(angular.toJson($scope.data));
          	//$scope.cart.push($scope.data);
          	console.log(angular.toJson($scope.cart));
          	//$rootScope.totalitems=$rootScope.totalitems+1;
            //return $scope.data.wifi;

          }
        }
      }
    ]
  });
  }

};

})

.controller('notificationctrl',["$scope","$state","$window","$http","$ionicPopup","_" ,function($scope,$state,$window,$http,$ionicPopup,_) {

$scope.$on('new Notification',function(){
  $scope.showNotification();
});
    $scope.showNotification=function(){

        $scope.notes= localStorage.notification?angular.fromJson(localStorage.notification): [];
        $scope.notes.reverse();

    } ;

     $scope.deleteNote=function(n){

        $scope.notes= angular.fromJson(localStorage.notification)|| [];
       
   
     var status = _.reject($scope.notes, function(all) {

                return all.payload.title == n.payload.title && all.payload.body == n.payload.body ;
            });
     $scope.notes=status;
     
    window.localStorage.notification = JSON.stringify(status);
     $scope.notes.reverse();
  


    } ;

  $scope.showNotification();
}])

.controller("ShoppingCartCtrl", ["$scope","$rootScope","$state", "CartService", "OfferService" , "$ionicActionSheet", "$ionicPopup", "$timeout","_", function($scope,$rootScope,$state, CartService, OfferService , $ionicActionSheet, $ionicPopup,$timeout, _) {
    $scope.products = CartService.getCartItems();
    $scope.isDisabled = false;
     $scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal'));
     $scope.$on('CartChange',function(event, args)
    {
    	$scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal'));
        $scope.products = CartService.getCartItems();

    });
    $scope.removeProductFromCart = function(test) {
        $ionicActionSheet.show({
            destructiveText: "Remove from cart",
            cancelText: "Cancel",
            cancel: function() {
                return !0;
            },
            destructiveButtonClicked: function() {
                return CartService.removeCartItem(test), $scope.products = CartService.getCartItems(), $scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal')), !0;
            }
        });};
       $scope.roundToTwo= function (num) {    
    			return +(Math.round(num + "e+2")  + "e-2");
		};
    $scope.$on('priceChange', function (event, args) {
		//CartService.getListPrices('/getVegetablePrices',$scope.vegetables).then(function(res){$scope.vegetables=res;},function(err){console.log("There is an error ");}) ;
		CartService.CartChange().then(function(res){console.log(res);

			$timeout(function(){$scope.$apply(function(){ $scope.products = CartService.getCartItems();$scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal')); });});},function(err){console.log("there is an error");});

	});
	$scope.GetOffer = function () {
        $scope.offerinfo=JSON.parse(localStorage.getItem('offerdetails'));
        $scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal'));
        $scope.discont=JSON.parse(localStorage.getItem('discont'));
            
    };
    $scope.GetOffer();
    $scope.$on('OfferChange', function (event, args) {
        $scope.GetOffer();     
    });
    $scope.offerCheck = function(offer){
    	$scope.isDisabled = true;
    	var phnumber=$rootScope.globals.currentUser.username;
    	var offerdata={'offer':offer,'mobile':phnumber};
        OfferService.getOfferDetails(angular.toJson(offerdata)).then(function(res){
           var info=res;
            $scope.offerdetails=info[0];
            var total=$rootScope.sumofAll;
            var discont;
            if ($scope.offerdetails && $scope.offerdetails.offer_criteria<total) {
            localStorage.setItem('offerdetails', JSON.stringify($scope.offerdetails));
                if($scope.offerdetails.offer_unit === 'rs'){
                    discont=$scope.offerdetails.offer_amt;
                    total=total-($scope.offerdetails.offer_amt);
                    localStorage.setItem('discont', JSON.stringify(discont));
                    $rootScope.sumofAll=total;
                }
                else
                {
                    discont=(total*(($scope.offerdetails.offer_amt)/100));
                    total=total-(total*(($scope.offerdetails.offer_amt)/100)).toFixed(2);
                    localStorage.setItem('discont', JSON.stringify(discont));
                    $rootScope.sumofAll=total;
                }
            
            $rootScope.$broadcast('OfferChange', { message: "test" });
        }
        else{
            $ionicPopup.alert({
                 title: 'Sorry',
                 template:"This Offer is not applicable. You can try again Later." });
            $scope.isDisabled = false;
        
        }

        },function(err){$ionicPopup.alert({
                 title: 'Sorry',
                 template:"You have entered invalid coupon code." });
        }) ;

    };

function getSumofAll(allCartItems){
			
		var sumofAll=0;	
		for(i=0;i<allCartItems.length;i++){
				sumofAll=sumofAll + (allCartItems[i].quantity * allCartItems[i].price);
		}
		//alert(sumofAll);
    return sumofAll;
}
    $scope.go = function() {$state.go('app.shipping');};
}])




.controller("ShippingCtrl", ["$scope","$state","$rootScope","address_data", "time_data","States_data","Cities_data","Areas_data","$ionicLoading","$ionicPopup","CartService", "$filter","_", function($scope, $state,$rootScope, address_data,time_data,States_data,Cities_data,Areas_data, $ionicLoading,$ionicPopup,CartService, $filter,_) {
    //console.log(angular.toJson(address_data));
    $scope.$on('$ionicView.enter', function(){
		
               $ionicLoading.hide();
	});

		
    $scope.user = address_data;
    $scope.newaddress=false;
    $scope.isDisabled = false;

    $scope.alltimings =  _.where( time_data, { id:parseInt($scope.user.area)} );
         $scope.states =  States_data  || [
	        { id: 1, name: 'Maharashtra' },
	        { id: 2, name: 'MP' },
	        { id: 3, name: 'Gujarat' },
	        { id: 4, name: 'UP' }
	      ];
	   	  
	      $scope.citys = Cities_data  || [
	        { id: 1, name: 'Pune' },
	        { id: 2, name: 'Mumbai' },
	        { id: 3, name: 'Nagpur' },
	        { id: 4, name: 'Nasik' }
	      ];
	      $scope.Areas = Areas_data  || [
	        { id: 1, name: 'Area 1' },
	        { id: 2, name: 'Area 2' },
	        { id: 3, name: 'Area 3' },
	        { id: 4, name: 'Area 4' }
	      ];
	       $scope.delivery_time = time_data  || [
	        { id: 1, name: 'Area 1' },
	        { id: 2, name: 'Area 2' },
	        { id: 3, name: 'Area 3' },
	        { id: 4, name: 'Area 4' }
	      ];

	      $scope.GetSelectedArea=function(area){

	      	$scope.alltimings =  _.where( time_data, { id:parseInt(area)} );
		         
		    
	      };
	      $scope.GetSelectedArea($scope.user.area);

			Array.prototype.remove = function(value) {
			       this.splice(this.indexOf(value), 1);
			       return true;
			};

			if ($scope.alltimings.length === 0) {
				$scope.alltimings.push({"id":10,"name":"5:30AM to 10:30AM"});
			}
			var availabledelivertoday=0;
			angular.forEach($scope.alltimings, function(d){
				if (d.name == "Deliver Today") {
					availabledelivertoday=1;

				}			
			});

			if (availabledelivertoday === 0) {
				$scope.alltimings.push({"id":11,"name":"Deliver Today"});
			}
			
			$scope.time = '3:00 PM';
			var now = new Date();
			var nowTime = new Date((now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + now.getHours()+":"+now.getMinutes());
			var userTime = new Date((now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + $scope.time);
			if ((nowTime.getTime() > userTime.getTime()) || (nowTime.getTime() == userTime.getTime())) {
			       $scope.alltimings.remove("Deliver Today");
			}



     $scope.removeProductFromCart = function(test) {
        $ionicActionSheet.show({
            destructiveText: "Remove from cart",
            cancelText: "Cancel",
            cancel: function() {
                return !0;
            },
            destructiveButtonClicked: function() {
                return CartService.removeCartItem(test), $scope.products = CartService.getCartItems(), !0;
            }
        });};

      
   
     $scope.go = function() { 
     	$scope.isDisabled = true;
     	$ionicLoading.show({
			template: 'Please Wait...'
		});
     		console.log("all order data" + JSON.stringify(CartService.getCartItems()));
          	$scope.user.deliveryTime=$scope.user.deliveryTime.name;
          	delete $scope.user.onesignal_token;
          	delete $scope.user.contacts;
	  	CartService.ConfirmOrder(CartService.getCartItems(),$scope.user	).then(function(res){
		var total=$rootScope.sumofAll;
		$rootScope.sumofAll=0;
		localStorage.removeItem('lsallCartItems');
		localStorage.removeItem('offerdetails');
		localStorage.removeItem('discont');
		$rootScope.currentOrders='';
		$scope.isDisabled = false;

		$state.go('app.thankyou',{delivery_time:$scope.user.deliveryTime.name,total:total});

		},function(error){ console.log("error "+ error);$scope.isDisabled = false;});  	
     
     };
}])

.controller("ThankyouCtrl", ["$scope", "$stateParams", "$ionicLoading","_", function($scope,$stateParams, $ionicLoading, _) {
 $ionicLoading.hide();  
$scope.delivery_time=$stateParams.delivery_time;
$scope.total=$stateParams.total;
$scope.facebook=function(){//alert("facebook");
	window.plugins.socialsharing.shareViaFacebook('Check the vegetablebazar ', '' , ' https://www.facebook.com/vegetablebazar/', function() {console.log('share ok');}, function(errormsg){alert("You Don't have whatsapp installed");});
	
};
$scope.whatsApp=function(){//alert("whatsapp");
	window.plugins.socialsharing.shareViaWhatsApp('Check the vegetablebazar ', '' , ' https://www.facebook.com/vegetablebazar/', function() {console.log('share ok');}, function(errormsg){alert("You Don't have facebook app installed");});
	};
/*$scope.instagram=function(){alert("iiinsta");
	window.plugins.socialsharing.shareViaInstagram('Check the vegetablebazar ',  ' https://www.instagram.com/vegetablebazar/  ', function() {console.log('share ok');}, function(errormsg){alert(errormsg);});
	};*/

   
}])
   

.controller("LogoutCtrl", ["$scope","$rootScope","$ionicHistory","AuthService","$state","$timeout","$window",  "_", function($scope,$rootScope,$ionicHistory,AuthService,$state,$timeout, $window, _) {
    
  var logout=function() {AuthService.ClearCredentials();
  	
	localStorage.removeItem('lsallCartItems');
	localStorage.removeItem('offerdetails');
	localStorage.removeItem('discont');
	$rootScope.currentOrders='';
  	localStorage.removeItem('lsallFruits');
  	localStorage.removeItem('lsallVegetables');
  	localStorage.removeItem('globals');
  	$window.localStorage.clear();
    $ionicHistory.clearCache();
    $ionicHistory.clearHistory();
    $rootScope.sumofAll=0;
    $state.go('auth.walkthrough');
};

$timeout(function () {logout();},100);

}])
   
;
