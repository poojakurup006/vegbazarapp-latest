angular.module('vegetablebazarapp.services', [])

// PUSH NOTIFICATIONS
.service('OfferService', function($http, $q,$rootScope,$window, $ionicLoading,SERVER_URL){
	this.getOfferDetails=function(details){
		var deferred = $q.defer();
		
		var req={
               method: 'GET',
               url: SERVER_URL+'/getOfferDetails',
               headers: {
                         'Content-Type': 'application/json',
                         'data': details
                        }
                        /*dataType : 'application/json',
                         data: details*/

            };
		$http(req)
		.success(function(data) {
			deferred.resolve(data);
		})
		.error(function(data) {
			deferred.reject(data);
		});

		return deferred.promise;
	};

})

.service('ContactInfoService', function($http, $q,$rootScope,$window, $ionicLoading,SERVER_URL){

	this.getContactInfo=function(data){
            var deferred =$q.defer();
			console.log("Contact Information");
			var req={method: 'GET',url: SERVER_URL+'/getContactInfo',headers: {'Content-Type': 'application/json',data:angular.toJson(data)}};
			$http(req).success(function (d) {
			                    deferred.resolve(d);
					}).error( function(data){
						deferred.reject(data); 
			});
				return	deferred.promise;
		};
})

.service('SocketService', ['socketFactory',function (socketFactory){
        return socketFactory({

            ioSocket: io.connect('https://www.vegetablebazar.com:20000')

        });
    }])
.service('PushNotificationsService', function ($rootScope,RequestsService, $cordovaPush, NodePushServer, GCM_SENDER_ID,SERVER_URL){
	/* Apple recommends you register your application for push notifications on the device every time it’s run since tokens can change. The documentation says: ‘By requesting the device token and passing it to the provider every time your application launches, you help to ensure that the provider has the current token for the device. If a user restores a backup to a device other than the one that the backup was created for (for example, the user migrates data to a new device), he or she must launch the application at least once for it to receive notifications again. If the user restores backup data to a new device or reinstalls the operating system, the device token changes. Moreover, never cache a device token and give that to your provider; always get the token from the system whenever you need it.’ */
	this.register = function() {
		var config = {};

		// ANDROID PUSH NOTIFICATIONS
		if(ionic.Platform.isAndroid())
		{
			config = {
				"senderID": GCM_SENDER_ID
			};
			RequestsService.register();
			$cordovaPush.register(config).then(function(result) {
				// Success
				console.log("$cordovaPush.register Success");
				console.log(result);
			}, function(err) {
				// Error
				console.log("$cordovaPush.register Error");
				console.log(err);
			});

			$rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
				console.log(JSON.stringify([notification]));
				switch(notification.event)
				{
					case 'registered':
						if (notification.regid.length > 0 ) {
							RequestsService.register(notification.regid);
							console.log('registration ID = ' + notification.regid);
							NodePushServer.storeDeviceToken("android", notification.regid);
						}
						break;

					case 'message':
						if(notification.foreground == "1")
						{
							console.log("Notification received when app was opened (foreground = true)");
						}
						else
						{
							if(notification.coldstart == "1")
							{
								console.log("Notification received when app was closed (not even in background, foreground = false, coldstart = true)");
							}
							else
							{
								console.log("Notification received when app was in background (started but not focused, foreground = false, coldstart = false)");
							}
						}

						// this is the actual push notification. its format depends on the data model from the push server
						console.log('message = ' + notification.message);
						break;

					case 'error':
						console.log('GCM error = ' + notification.msg);
						break;

					default:
						console.log('An unknown GCM event has occurred');
						break;
				}
			});

			// WARNING: dangerous to unregister (results in loss of tokenID)
			// $cordovaPush.unregister(options).then(function(result) {
			//   // Success!
			// }, function(err) {
			//   // Error
			// });
		}

		if(ionic.Platform.isIOS())
		{
			config = {
				"badge": true,
				"sound": true,
				"alert": true
			};

			$cordovaPush.register(config).then(function(result) {
				// Success -- send deviceToken to server, and store for future use
				console.log("result: " + result);
				NodePushServer.storeDeviceToken("ios", result);
			}, function(err) {
				console.log("Registration error: " + err);
			});

			$rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
				console.log(notification.alert, "Push Notification Received");
			});
		}
	};
})


.service('AuthService', function ($rootScope, $http, $q,$timeout,$window, SERVER_URL){

	this.doSignUp=function(details){
		var deferred = $q.defer();
		
		var req={
               method: 'POST',
               url: SERVER_URL+'/doSignUp',
               headers: {
                         'Content-Type': 'application/json',
                         'data':details
                                      
                         }
                        
             };
		$http(req)
		.success(function(data) {
			deferred.resolve(data);
		})
		.error(function(data) {
			deferred.reject(data);
		});

		return deferred.promise;
	};

	this.doLogin=function (data){
 
            var deferred = $q.defer();
           //	 $timeout(function () {
                var response;
                 
                // var yy='abcr94zwtRvLY7ihcQMpv';
                //var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64;}else if(isNaN(i)){a=64;}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a);}return t;},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r);}if(a!=64){t=t+String.fromCharCode(i);}}t=Base64._utf8_decode(t);return t;},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128);}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128);}}return t;},_utf8_decode:function(e){var t="";var n=0;var r;var c1;var c2; r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++;}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2;}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3;}}return t;}}; 
				//var encodedPassword=Base64.encode( password);                
				var req={
			               method: 'GET',
			               url: SERVER_URL+'/doLogin',
			               headers: {
			                         'Content-Type': 'application/json',data:data
			                                      
			                         }
			               
			             };
					$http(req).success(function (res) {
						 console.log(angular.toJson(res));
                             var user1=angular.toJson(res);
                             console.log(user1.password);
	                 		 console.log("data ======"+data);
	                 		 
            if (res && res.length>0 && user1 !== null && user1.password === data.password) {


							
                        // if (user1 !== null && user1.passMD5 == encodedPassword) {
                            response = { success: true, isVerified : res[0].isVerified};
							console.log("success responce"+angular.toJson(response));
                        
                        } else {
                            response = { success: false, message: 'Username or password is incorrect' };
							console.log(" false rresponse  " + angular.toJson(response));
                        }
						deferred.resolve(response);
                       // callback(response);
                    }).error( function(data){deferred.reject(data);});
            //}, 1000);
          return deferred.promise;
	};
   this.SetCredentials=  function (username, password) {
			var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64;}else if(isNaN(i)){a=64;}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a);}return t;},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r);}if(a!=64){t=t+String.fromCharCode(i);}}t=Base64._utf8_decode(t);return t;},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128);}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128);}}return t;},_utf8_decode:function(e){var t="";var n=0;var r;var c1;var c2; r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++;}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2;}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3;}}return t;}};             
			var authdata = Base64.encode(username + ':' + password);
      
            $rootScope.globals = {
                currentUser: {
                    username: username,
                    authdata: authdata
                }
            };

          
            $http.defaults.headers.common.Authorization = 'Basic ' + authdata;
			localStorage.globals= angular.toJson($rootScope.globals);
};
  this.ClearCredentials =function () {
            $rootScope.globals = '';
            localStorage.globals=angular.toJson({});
            $http.defaults.headers.common.Authorization = 'Basic';
        };
this.getAddressData =function () {
	var deferred = $q.defer();
           var data = $rootScope.globals.currentUser.username;
console.log(data);
            
           var req={method: 'GET', url: SERVER_URL+'/getAddress',
			               headers: {'Content-Type': 'application/json',data:data}
       				};
					$http(req).success(function (res) {
						         $rootScope.addressData=res;
						         console.log("inside service" + $rootScope.addressData);
						         deferred.resolve(res);
						    }).error( function(data){
							console.log(angular.toJson(res));
						    	deferred.reject(data);
						    	});
            //}, 1000);
          return deferred.promise;
	};


	this.submitOTP=function(otp,mobile){
		var deferred = $q.defer();
       //ar user = 
        var data={mobile:mobile,otp:otp};
       
        var req={method: 'GET', url: SERVER_URL+'/submitOTP',headers: {'Content-Type': 'application/json','data':JSON.stringify(data)}};
        console.log(req);
					$http(req).success(function (res) {
						       deferred.resolve(res);
						    }).error( function(data){deferred.reject(data);
						    	deferred.reject(data);});
            //}, 1000);
          return deferred.promise;
	};
	this.resendOTP=function(mobile){
		var deferred = $q.defer();
        //var data = $rootScope.globals.currentUser.username;
       /* var userOTP={mobile:data};*/
        var req={method: 'GET', url: SERVER_URL+'/resendOTP',headers: {'Content-Type': 'application/json',data:mobile}};
					$http(req).success(function (res) {
						       deferred.resolve(res);
						    }).error( function(data){deferred.reject(data);
						    	deferred.reject(data);});
            //}, 1000);
          return deferred.promise;
	};

	this.getStates=function(){
		var deferred = $q.defer();
        
       
        var req={method: 'GET', url: SERVER_URL+'/getStates',headers: {'Content-Type': 'application/json'}};
					$http(req).success(function (res) {
						       deferred.resolve(res);
						       
						    }).error( function(data){
						    	 alert("error");deferred.reject(data);
						    	});
            //}, 1000);
          return deferred.promise;
	};
	this.getMasterCities=function(){
		var deferred = $q.defer();
        
        
        var req={method: 'GET', url: SERVER_URL+'/getMasterCities',headers: {'Content-Type': 'application/json'}};
					$http(req).success(function (res) {
						
						       deferred.resolve(res);
						    }).error( function(data){deferred.reject(data);
						    	deferred.reject(data);});
            //}, 1000);
          return deferred.promise;
	};
	this.getCities=function(){
		var deferred = $q.defer();
        
        
        var req={method: 'GET', url: SERVER_URL+'/getCities',headers: {'Content-Type': 'application/json'}};
					$http(req).success(function (res) {
						
						       deferred.resolve(res);
						    }).error( function(data){deferred.reject(data);
						    	deferred.reject(data);});
            //}, 1000);
          return deferred.promise;
	};
	this.getAreas=function(){
		var deferred = $q.defer();
		
        var req={method: 'GET', url: SERVER_URL+'/getAreas',headers: {'Content-Type': 'application/json',data:angular.toJson($rootScope.globals.currentUser.username)}};
					$http(req).success(function (res) {
						
						       deferred.resolve(res);
						    }).error( function(data){deferred.reject(data);
						    	deferred.reject(data);});
            //}, 1000);
          return deferred.promise;
	};
	this.getStatus=function(){
		var deferred = $q.defer();
		
        var req={method: 'GET', url: SERVER_URL+'/getStatus',headers: {'Content-Type': 'application/json'}};
					$http(req).success(function (res) {
						       deferred.resolve(res);
						       
						    }).error( function(data){deferred.reject(data);
						    	deferred.reject(data);});
            //}, 1000);
          return deferred.promise;
	};

	
	this.saveAddress =function (newAddess) {
	var deferred = $q.defer();
           var data = $rootScope.globals.currentUser.username;
console.log(data);
            
           var req={method: 'GET', url: SERVER_URL+'/saveAddress',
			               headers: {'Content-Type': 'application/json',data:newAddess}
       				};
					$http(req).success(function (res) {
						       console.log(angular.toJson(res));
						        deferred.resolve(res);
						    }).error( function(data){
							console.log(angular.toJson(data));
						    	deferred.reject(data);
						    	});
            //}, 1000);
          return deferred.promise;
	};
	//changePassword
	this.changePassword =function (newpassword) {
	var deferred = $q.defer();
           var data = $rootScope.globals.currentUser.username;
			console.log(data);
            
           var req={method: 'GET', url: SERVER_URL+'/changePassword',
			               headers: {'Content-Type': 'application/json',data:newpassword}
       				};
					$http(req).success(function (res) {
						        var user1=angular.toJson(res);
						       console.log(angular.toJson(res));
						        deferred.resolve(res);
						    }).error( function(data){
							console.log(angular.toJson(data));
						    	deferred.reject(data);
						    	});
            //}, 1000);
          return deferred.promise;
	};
                             
})


.service('CartService', function (_,$rootScope, $http,$timeout, $q, SERVER_URL){
   var allCartItems=[];
   var self = this;
   $rootScope.sumofAll=!_.isUndefined(window.localStorage.lsallCartItems) ?
														getSumofAll(JSON.parse(window.localStorage.lsallCartItems)) : 0;

	self.addCartItem = function(cartItem) {
		
		var allCartItems = !_.isUndefined(window.localStorage.lsallCartItems) ?
														JSON.parse(window.localStorage.lsallCartItems) : [];

		

		var existing_item = _.find(allCartItems, function(post){ return ((post.id == cartItem.id) && (post.name===cartItem.name)); });
		
		if(!existing_item){

			allCartItems.push({
				id: cartItem.id,
				name : cartItem.name,
				hindi_name : cartItem.hindi_name,
				quantity: cartItem.quantity,
				image_domain :cartItem.image_domain,
				image :cartItem.image,
				price :cartItem.price,
				quan :cartItem.quan,
				unit :cartItem.unit/*,
				excerpt: bookmark_post.contentSnippet*/
			});
			$rootScope.totalitems=allCartItems.length;
			$rootScope.sumofAll=getSumofAll(allCartItems);
		}
		else{
		 var updatedCartItems=	_.extend(_.findWhere(allCartItems, { id: cartItem.id,name:cartItem.name }), cartItem);
		$rootScope.sumofAll=getSumofAll(allCartItems);
		 console.log(JSON.stringify(updatedCartItems));
			//return existing_item.name + "already added :" + existing_item.quantity;
		}

		window.localStorage.lsallCartItems = JSON.stringify(allCartItems);
		localStorage.removeItem('offerdetails');
         localStorage.removeItem('discont');
         $rootScope.$broadcast('OfferChange', { message: "test" });
		$rootScope.$broadcast("new-cart-item");

	};
	self.CartChange= function () {
		 //$scope.priceList = args.priceList;
		 //console.log($scope.message);
		 
		var deferred = $q.defer();
		 var products;
		
		getFruitPrices().then(function(res){
			 var fruitprices=res;
			 products=self.getCartItems();
			if(fruitprices && fruitprices.length>0 && products.length>0){
		       	 		
		 			var combined = _.map(products, function(base){
		                        return _.extend(base, _.findWhere( fruitprices, { id: base.id,name : base.name} ));
		          });
		 			products= combined;
		 			$rootScope.sumofAll=getSumofAll(products);
		 			window.localStorage.lsallCartItems = JSON.stringify(products);

		 			getvegetablePrices().then(function(res){ var vegprices=res;
		 	
		 	 
				 	if(vegprices && vegprices.length>0 && products.length>0){
				       	 		
				 				var vcombined = _.map(products, function(base){
				                        return _.extend(base, _.findWhere( vegprices, { id: base.id,name : base.name} ));
				          });
				 			products= vcombined;
				 			$rootScope.sumofAll=getSumofAll(products);
				 			window.localStorage.lsallCartItems = JSON.stringify(products);
				 			deferred.resolve(products);
				 		}
				 	},function(err){deferred.reject(err);console.log("There is an error ");}) ;
			}
		},function(err){ deferred.reject(err);console.log("There is an error ");}) ;
	
		 	//alert(angular.toJson(window.localStorage.lsallCartItems));
		   return deferred.promise;
		 	
		 	
	};	
	/*self.getVegetablePrices=function(){
		var deferred = $q.defer();
		$timeout(function(){ 
			      
			      var pricereq={ method: 'GET',url: SERVER_URL+'/getVegetablePrices',headers: {'Content-Type': 'application/json',data:$rootScope.globals.currentUser.username}};
			       $http(pricereq).success(function (res) {  
			       	deferred.resolve(res); }).error( function(data){deferred.reject(data); }); 
			    },200); return deferred.promise; 
	}*/
	self.getListPrices = function(listName,listData){
		var deferred = $q.defer();
		 $timeout(function(){ 
		      
		      var pricereq={ method: 'GET',url: SERVER_URL+ listName ,headers: { 'Content-Type': 'application/json',data:$rootScope.globals.currentUser.username}};
		       console.log(angular.toJson(pricereq));
		       $http(pricereq).success(function (res) { var combined={};
		       	if(res && res.length>0){
		       	 		//$scope.$emit('CartChange',{'priceList':res});
		 				 combined = _.map( listData, function(base){
		                        return _.extend(base, _.findWhere( res, { id: base.id,name : base.name} ));
		          });}
		       	deferred.resolve(combined); }).error( function(data){deferred.reject(data); });
		    },100);  return deferred.promise; 
	};

function getSumofAll(allCartItems){
			
		var sumofAll=0;	
		for(i=0;i<allCartItems.length;i++){
				sumofAll=sumofAll + roundToTwo(allCartItems[i].quantity * allCartItems[i].price);
		}
			var oldTotal=sumofAll;
            localStorage.setItem('oldTotal', JSON.stringify(oldTotal));
		var offerdetails=angular.fromJson(window.localStorage.offerdetails);
		if (offerdetails && offerdetails.offer_criteria<sumofAll) {
			
			var discont=0; var total=sumofAll;
			if(offerdetails.offer_unit === 'rs'){
                discont=offerdetails.offer_amt;
                total=total-(offerdetails.offer_amt);
                localStorage.setItem('discont', JSON.stringify(discont));
            }
            else
            {
                discont=(total*(($scope.offerdetails.offer_amt)/100));
                total=total-(total*((offerdetails.offer_amt)/100)).toFixed(2);
                localStorage.setItem('discont', JSON.stringify(discont));
            }
            sumofAll=total;
		}
		if ( sumofAll < 0 || sumofAll === 0) {
            localStorage.removeItem('offerdetails');
            localStorage.removeItem('discont');
            $rootScope.$broadcast('OfferChange', { message: "test" });
            sumofAll=0;
        }
        if (offerdetails && offerdetails.offer_criteria>sumofAll) {
        	localStorage.removeItem('offerdetails');
            localStorage.removeItem('discont');
            $rootScope.$broadcast('OfferChange', { message: "test" });
        }
    return Math.ceil(sumofAll);
}
function roundToTwo(num) {    
    			return +(Math.round(num + "e+2")  + "e-2");
		}
self.checkExistingItem = function(cartItem) {
	var allCartItems = !_.isUndefined(window.localStorage.lsallCartItems) ?
													JSON.parse(window.localStorage.lsallCartItems) : [];
		
		var existing_item = _.find(allCartItems, function(post){ console.log((post.id == cartItem.id) && (post.name===cartItem));return ((post.id == cartItem.id) && (post.name===cartItem.name)); });
		if(existing_item){return existing_item;}
		else {return;}
};

	self.removeCartItem = function(remove_item) {
		var allCartItems = !_.isUndefined(window.localStorage.lsallCartItems) ?
													JSON.parse(window.localStorage.lsallCartItems) : [];
													/*alert(allCartItems.length);*/
		 var status = _.reject(allCartItems, function(allCartItem) {

                return allCartItem.id == remove_item.id && allCartItem.name == remove_item.name ;
            });
		 
		 $rootScope.totalitems= status.length; // allCartItems.length;
		 $rootScope.sumofAll=getSumofAll(status);
		 localStorage.removeItem('offerdetails');
         localStorage.removeItem('discont');
         $rootScope.$broadcast('OfferChange', { message: "test" });
		window.localStorage.lsallCartItems = JSON.stringify(status);
	};

	self.getCartItems = function(posts) {
		var allCartItems =!_.isUndefined(window.localStorage.lsallCartItems) ?
													JSON.parse(window.localStorage.lsallCartItems) : [];
				return allCartItems;
		
	};

	getFruitPrices =function(){ 
      var deferred = $q.defer();
       $timeout(function(){ 
		      
   //var SERVER_URL='http://localhost:3000';
       pricereq={ method: 'GET',url: SERVER_URL+'/getFruitPrices',headers: {'Content-Type': 'application/json',data:$rootScope.globals.currentUser.username}};
       $http(pricereq).success(function (res) {   deferred.resolve(res);}).error( function(data){deferred.reject(data); });
       },100);
       return deferred.promise; 
    };
    getvegetablePrices =function(){ 
      var deferred = $q.defer();
   	$timeout(function(){ 
       pricereq={ method: 'GET',url: SERVER_URL+'/getVegetablePrices',headers: {'Content-Type': 'application/json',data:$rootScope.globals.currentUser.username}};
       $http(pricereq).success(function (res) {   deferred.resolve(res); }).error( function(data){deferred.reject(data); }); 
        },100);return deferred.promise; 
    };


	self.ConfirmOrder=function(allproducts,user){
		//var allproducts =;
        var offerdet=JSON.parse(localStorage.getItem('offerdetails'));
        var couponcode=(offerdet!==null) ? (offerdet.offercode) : 'No Coupon';
        var oldTotal=(localStorage.getItem('oldTotal')!==null) ? JSON.parse(localStorage.getItem('oldTotal')) : [];

		var alldata={'order_detail':JSON.stringify(allproducts),'mobile':user.mobile , 'coupon_code':couponcode , 'total_before_coupon_apply':oldTotal , 'total' : $rootScope.sumofAll,'delivery_time':user.deliveryTime,'status':'pending','customer_details':JSON.stringify(user)};
		var deferred = $q.defer();
		console.log("all order data from service" + JSON.stringify(alldata));
        var req={method: 'GET', url:SERVER_URL+'/confirmOrder',params: {data:JSON.stringify(alldata)}};
					$http(req).then(function(res){
						       console.log("inside service success" + angular.toJson(res));
						       deferred.resolve(res);
						       },function(err){
						       	console.log("inside service error" + angular.toJson(err));
						       	deferred.reject(err);
						    	});
            //}, 1000);
          return deferred.promise;
	};

	self.getDeliveryTime=function(){
		var deferred = $q.defer();
		//console.log("all order data from service" + JSON.stringify(alldata));
        var req={method: 'GET', url: SERVER_URL+'/getDeliveryTime',headers: {'Content-Type': 'application/json',data:angular.toJson($rootScope.globals.currentUser.username)}};
		$http(req).success(function (res) {  deferred.resolve(res);        
						    }).error( function(data){deferred.reject(data);	});
          return deferred.promise;
	};		
	

})


.service('RequestsService', function($http, $q,$rootScope, $ionicLoading,SERVER_URL){

  //for server
  var base_url = 'your-server.com';

  function register(device_token,onesignal_token){
            if($rootScope.globals.currentUser){
    var deferred = $q.defer();
    $ionicLoading.show();

    $http.post(SERVER_URL + '/register', {'device_token': device_token,'onesignal_token':onesignal_token,'userID': $rootScope.globals.currentUser})
     .success(function(response){
      
      $ionicLoading.hide();
      deferred.resolve(response);
      
     })
     .error(function(data){
      deferred.reject(); 
     });
    

    return deferred.promise;   
   }
  }

		function contacts(allContacts){
            if($rootScope.globals.currentUser){
				var deferred = $q.defer();
				$ionicLoading.show();
         		getAllContacts();
				$http.post(SERVER_URL + '/contacts', {'contacts': allContacts,'userID': $rootScope.globals.currentUser})
					.success(function(response){
						
						$ionicLoading.hide();
						deferred.resolve(response);
						
					})
					.error(function(data){
						deferred.reject();	
					});
				

				return deferred.promise;			
			}
		}

		function getAllContacts(){
				var deferred = $q.defer();
			 if(  navigator.contacts){
                            var options      = new ContactFindOptions();
			                    //options.filter   = "Bob";
			                    options.multiple = true;
			                    options.desiredFields = ['id','displayName','phoneNumbers'];
			                    options.hasPhoneNumber = true;
			                   navigator.contacts.find(['id', 'displayName'],function(allContacts) { //omitting parameter to .find() causes all contacts to be returned
			                     $q.resolve(allContacts) ;
			                      //console.log(angular.toJson(allContacts));
			                     
			                    },function(err){ q.reject(err);console.log(angular.toJson(err));},options);
			                  }
		}


		return {
			register: register,
			contacts:contacts,
			getAllContacts:getAllContacts
		};
	})




;
